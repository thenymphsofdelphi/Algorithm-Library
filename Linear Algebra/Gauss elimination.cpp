vector <modular> solve_linear_equations(vector <vector <modular>> a, vector <modular> b){
    int n = a.size();
    vector <modular> x(n, (modular)0);
    For(i, 0, n){
        b[i] /= a[i][i];
        FordE(j, n - 1, i){
            a[i][j] /= a[i][i];
        }
        For(k, i + 1, n){
            b[k] -= b[i] * a[k][i];
            FordE(j, n - 1, i){
                a[k][j] -= a[i][j] * a[k][i];
            }
        }
    }
    FordE(i, n - 1, 0){
        x[i] = b[i];
        FordE(k, i - 1, 0){
            b[k] -= x[i] * a[k][i];
        }
    }
    return x;
}
