# Flows

## Overview, Definitions

A **network** is a directed graph $`G`$, with set of vertices $`V`$, set of edges $`E`$, and also a **capacity** function $`c: V^2 \rightarrow [0, \infty)`$. 

The capacity function $`c(u, v)`$ is positive only for $`(u, v) \in E`$.

We denote the network as $`G = (V, c)`$.

A **flow network** is a network with two special vertices called **source** (typically $`s`$) and **sink** (typically $`t`$).

A flow network must also satisfies $`\forall v \in V, c(v, s) = c(t, v) = 0`$ (there is no edge go to $`s`$, and there is no edge go from $`t`$).

We denote the flow network as $`G = (V, c, s, t)`$.

A **flow** in a flow network is a function $`f: V^2 \rightarrow [0, \infty)`$.

A flow must satisfies three conditions:

- **Skew symmetry**: Net flow from $`u`$ to $`v`$ is the opposite of the netflow from $`v`$ to $`u`$.

  $`\forall u, v \in V, f(u, v) = -f(v, u)`$.

- **Capacity constraint**: The flow of an edge cannot exceeds the capacity.

  $`\forall u, v \in V, f(u, v) \leq c(u, v)`$.

- **Flow conservation**: The sum of incoming flow of a vertex $`u \neq s, t`$ must equal to the sum of outgoing flow.

  $`\sum_{v \in V} f(v, u) = \sum_{v \in V} f(u, v)`$.

From those two above conditions, we can see that this equation holds: $`\sum_{u \in V} f(s, u) = \sum_{u \in V} f(u, t)`$.

The **value** of a flow is the sum of all flows from $`s`$, formally, $`\sum_{u \in V} f(s, u)`$.

A **maximum flow** is a flow with a maximum value, and this is what we wanted to find.

## Ford-Fulkerson method

Let's define some more thing.

A **residual capacity** in a network is a function $`c^*: V^2 \rightarrow [0, \infty)`$.

The residual capacity is simply the difference between capacity and flow, formally, $`c^*(u, v) = c(u, v) - f(u, v)`$.

From those residual capacity, we can create a **residual network**.

We denote the residual network as $`G^* = (V, c^*)`$.

An **augmenting path** is a simple path from $`s`$ to $`t`$ in the residual network, such that the residual capacity of each edge in the path is positive.

The **Ford-Fulkerson method** works as follow:

1. Set the flow of each edge to $`0`$.

2. While there exists an augmenting path in the residual network:

    1. Increase the flow along the path. 
    
       Let $`x`$ be the smallest value among the residual capacity of the edges in the path. Then we update $`f(u, v) \leftarrow f(u, v) + x`$ and $`f(v, u) \leftarrow f(v, u) - x`$ for every edge $`(u, v)`$ in the graph.

3. When there is no more augmenting path, the final flow is maximum.

This method do not specify how to find an augmenting path. DFS and BFS can be used, both have complexity $`\mathcal{O}(E)`$.

<details>
<summary> <em> Sidenote </em> </summary>

The actual complexity of DFS/BFS is $`\mathcal{O}(V + E)`$, but since $`E \geq V`$ (if not, there is some disconnected vertices, and we can remove them), we can simplified it to $`\mathcal{O}(E)`$.

</details>

Assuming that value of flow, denoted $`F`$, is integer, then the complexity of Ford-Fulkerson method is $`\mathcal{O}(EF)`$.

# Edmond-Karp algorithm

The **Edmond-Karp algorithm** is just Ford-Fulkerson method, but it specifies that BFS is used for finding augmenting path. Therefore, the shortest augmenting path (assuming each edge has length $`1`$) is found every iteration.

This algorithm has a runtime independent of Ford-Fulkerson method, which is $`\mathcal{O}(VE^2)`$.

An outline of the proof is:

- An augmenting path can be found in $`\mathcal{O}(E)`$.

- Everytime we increase the flow, one of the $`E`$ edges is **saturated** (ie. residual capacity is zero).

- The distance between $`s`$ and the saturated edge must be longer than the last time it was saturated.

- The length of a simple path is at most $`V`$.

# Dinic's algorithm

Some more boring definitions:

A **blocking flow** is a flow that satisfies every path from $`s`$ to $`t`$ has at least one saturated edge.

A **layered network** of a network $`G`$ is 




















