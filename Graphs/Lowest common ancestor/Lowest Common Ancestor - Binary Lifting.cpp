struct binary_lifting{
    vi height;
    vvi lift;
    
    void dfs_binlift(int u, int p, vi adj[]){
        lift[u][0] = p;
        for (int j = 1; j < lift[0].size(); j++){
            lift[u][j] = lift[lift[u][j - 1]][j - 1];
        }
        Fora(v, adj[u]){
            if (v == p){
                continue;
            }
            height[v] = height[u] + 1;
            dfs_binlift(v, u, adj);
        }
    }
    
    binary_lifting(int n, vi adj[]){
        height = vi (n + 1, 0);
        lift = vvi (n + 1, vi ((int)log2(n) + 1, 0));
        dfs_binlift(1, 1, adj);
    }
    
    int lca(int u, int v){
        if (height[u] < height[v]){
            swap(u, v);
        }
        for (int j = lift[0].size() - 1; j >= 0; j--){
            if (height[u] - (1 << j) >= height[v]){
                u = lift[u][j];
            }
        }
        if (u == v){
            return u;
        }
        for (int j = lift[0].size() - 1; j >= 0; j--){
            if (lift[u][j] != lift[v][j]){
                u = lift[u][j];
                v = lift[v][j];
            }
        }
        return lift[u][0];
    }
    
    int nearlca(int u, int v){
        if (height[u] <= height[v]){
            return -1;
        }
        assert(height[u] > height[v]);
        for (int j = lift[0].size(); j >= 0; j--){
            if (height[u] - (1 << j) > height[v]){
                u = lift[u][j];
            }
        }
        if (lift[u][0] != v){
            return -1;
        }
        return u;
    }
};