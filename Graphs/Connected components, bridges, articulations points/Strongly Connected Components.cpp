void dfs_toposort(int u, vi adj[], bool ck[], vi& vtoposort){
    ck[u] = 1;
    Fora(v, adj[u]){
        if (ck[v]) continue;
        dfs_toposort(v, adj, ck, vtoposort);
    }
    vtoposort.push_back(u);
    return;
}

void find_toposort(int n, vi adj[], vi& vtoposort){
    bool ck[n + 1];
    memset(ck, 0, sizeof(ck));
    ForE(i, 1, n){
        if (!ck[i]){
            dfs_toposort(i, adj, ck, vtoposort);
        }
    }
    reverse(bend(vtoposort));
    return;
}

void dfs_scc(int u, vi radj[], bool ck[], vi& vscc){
    ck[u] = 1;
    vscc.push_back(u);
    Fora(v, radj[u]){
        if (!ck[v]){
            dfs_scc(v, radj, ck, vscc);
        }
    }
}

int find_scc(int n, vi adj[], vvi& vsccs){
    vi radj[n + 1];
    ForE(u, 1, n){
        Fora(v, adj[u]){
            radj[v].push_back(u);
        }
    }
    vi vtoposort;
    find_toposort(n, adj, vtoposort);
    bool ck[n + 1];
    memset(ck, 0, sizeof(ck));
    vi vscc;
    ForE(i, 0, n - 1){
        if (!ck[vtoposort[i]]){
            vscc.clear();
            dfs_scc(vtoposort[i], radj, ck, vscc);
            vsccs.push_back(vscc);
        }
    }
    return vsccs.size();
}