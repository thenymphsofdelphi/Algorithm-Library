void dfs_toposort(int u, vi adj[], bool ck[], vi& vtoposort){
    ck[u] = 1;
    Fora(v, adj[u]){
        if (ck[v]) continue;
        dfs_toposort(v, adj, ck, vtoposort);
    }
    vtoposort.push_back(u);
    return;
}

void find_toposort(int n, vi adj[], vi& vtoposort){
    bool ck[n + 1];
    memset(ck, 0, sizeof(ck));
    ForE(i, 1, n){
        if (!ck[i]){
            dfs_toposort(i, adj, ck, vtoposort);
        }
    }
    reverse(bend(vtoposort));
    return;
}
