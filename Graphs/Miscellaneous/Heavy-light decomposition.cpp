struct Segment_Tree{
    int n;
    vi segmx, lazy;

    Segment_Tree(int n = 0): n(n) {
        segmx.resize(4 * n + 1);
        lazy.resize(4 * n + 1);
    }

    void Merge(int id, int l, int r){
        segmx[id] = max(segmx[id << 1], segmx[id << 1 | 1]);
    }

    void Push(int id, int l, int r){
        segmx[id << 1] += lazy[id];
        segmx[id << 1 | 1] += lazy[id];
        lazy[id << 1] += lazy[id];
        lazy[id << 1 | 1] += lazy[id];
        lazy[id] = 0;
    }

    void Build(int id, int l, int r){
        if (l == r){
            segmx[id] = 0;
            return;
        }
        int mid = (l + r) >> 1;
        Build(id << 1, l, mid);
        Build(id << 1 | 1, mid + 1, r);
        Merge(id, id << 1, id << 1 | 1);
    }

    void Update(int id, int l, int r, int u, int v, int val){
        if (v < l || r < u){
            return;
        }
        if (u <= l && r <= v){
            segmx[id] += val;
            lazy[id] += val;
            return;
        }
        Push(id, l, r);
        int mid = (l + r) >> 1;
        Update(id << 1, l, mid, u, v, val);
        Update(id << 1 | 1, mid + 1, r, u, v, val);
        Merge(id, l, r);
    }

    int Get_Max(int id, int l, int r, int u, int v){
        if (v < l || r < u){
            return 0;
        }
        if (u <= l && r <= v){
            return segmx[id];
        }
        Push(id, l, r);
        int mid = (l + r) >> 1;
        return max(Get_Max(id << 1, l, mid, u, v), Get_Max(id << 1 | 1, mid + 1, r, u, v));
    }
};

struct Heavy_Light_Decomposition{
    int n;
    vvi adj;
    Segment_Tree seg;
    vi par, sz, height, heavy, head, posseg;
    /*
    adj: adjacency list
    seg: segment tree
    sz[u]: size of subtree u
    height[u]: height of u
    par[u] = parent of u
    heavy[u] = heaviest child of u
    head[u] = head of chain that contains u
    posseg[u] = position of u in segment tree
    */

    Heavy_Light_Decomposition(int n = 0): n(n) {
        adj.resize(n + 1);
        seg = Segment_Tree(n);
        par.resize(n + 1);
        sz.resize(n + 1);
        height.resize(n + 1);
        heavy.resize(n + 1);
        head.resize(n + 1);
        posseg.resize(n + 1);
    }

    Heavy_Light_Decomposition(int n, vvi& adj): n(n), adj(adj) {
        seg = Segment_Tree(n);
        par.resize(n + 1);
        sz.resize(n + 1);
        height.resize(n + 1);
        heavy.resize(n + 1);
        head.resize(n + 1);
        posseg.resize(n + 1);
    }

    void Add_Edge(int u, int v){
        adj[u].push_back(v); adj[v].push_back(u);
    }

    void Dfs_Hld(int u, int p){
        par[u] = p;
        sz[u] = 1;
        height[u] = height[p] + 1;
        int mxsz = -1;
        Fora(v, adj[u]){
            if (v == par[u]){
                continue;
            }
            Dfs_Hld(v, u);
            sz[u] += sz[v];
            if (mxsz < sz[v]){
                mxsz = sz[v];
                heavy[u] = v;
            }
        }
    }

    void Decompose(int u, int h, int& idxposseg){
        head[u] = h;
        posseg[u] = ++idxposseg;
        if (heavy[u]){
            Decompose(heavy[u], h, idxposseg);
        }
        Fora(v, adj[u]){
            if (v == par[u] || v == heavy[u]){
                continue;
            }
            Decompose(v, v, idxposseg);
        }
    }

    void Init(int u = 1){
        Dfs_Hld(u, u);
        int idxposseg = 0;
        Decompose(u, u, idxposseg);
    }

    int Lca(int u, int v){
        while (head[u] != head[v]){
            if (height[head[u]] > height[head[v]]){
                swap(u, v);
            }
            v = par[head[v]];
        }
        if (height[u] > height[v]){
            swap(u, v);
        }
        return u;
    }

    void Update(int u, int v, int val){
        while (head[u] != head[v]){
            if (height[head[u]] > height[head[v]]){
                swap(u, v);
            }
            seg.Update(1, 1, n, posseg[head[v]], posseg[v], val);
            v = par[head[v]];
        }
        if (height[u] > height[v]){
            swap(u, v);
        }
        seg.Update(1, 1, n, posseg[u], posseg[v], val);
    }

    int Get_Max(int u, int v, int val){
        int ans = 0;
        while (head[u] != head[v]){
            if (height[head[u]] > height[head[v]]){
                swap(u, v);
            }
            ans = max(ans, seg.Get_Max(1, 1, n, posseg[head[v]], posseg[v]));
            v = par[head[v]];
        }
        if (height[u] > height[v]){
            swap(u, v);
        }
        ans = max(ans, seg.Get_Max(1, 1, n, posseg[u], posseg[v]));
        return ans;
    }
};