struct Eulerian_Graph_Dir{
    struct Edge{
        int u, v;
        bool reach;
        
        Edge(int u, int v, bool reach): u(u), v(v), reach(reach) {
            
        }
    };
    
    int n;
    vector <Edge> edges;
    vvi adj;
    vi deg, ptr;
    
    Eulerian_Graph_Dir(int n = 0): n(n) {
        adj.resize(n + 1);
        deg.resize(n + 1);
        ptr.resize(n + 1);
    }

    void Add_Edge(int u, int v){
        edges.push_back(Edge(u, v, 0));
        adj[u].push_back(isz(edges) - 1);
        deg[u]--; deg[v]++;
    }

    void Dfs_Eulerian_Dir(int u, vi& path){
        while (ptr[u] < isz(adj[u])){
            ptr[u]++;
            int i = adj[u][ptr[u] - 1];
            if (edges[i].reach){
                continue;
            }
            edges[i].reach = 1;
            Dfs_Eulerian_Dir(edges[i].v, path);
        }
        path.push_back(u);
        return;
    }

    bool Eulerian_Cycle(vi& path){
        ForE(i, 1, n){
            if (deg[i]){
                return 0;
            }
        }
        ForE(i, 1, n){
            if (isz(adj[i])){
                Dfs_Eulerian_Dir(i, path);
                break;
            }
        }
        ForE(i, 1, n){
            if (ptr[i] != isz(adj[i])){
                return 0;
            }
        }
        reverse(bend(path));
        return 1;
    }

    bool Eulerian_Path(vi& path){
        int pathbegin = -1, pathend = -1;
        ForE(i, 1, n){
            if (deg[i] < -1 || deg[i] > 1){
                return 0;
            }
            if (deg[i] == -1){
                if (pathbegin != -1){
                    return 0;
                }
                pathbegin = i;
            }
            if (deg[i] == 1){
                if (pathend != -1){
                    return 0;
                }
                pathend = i;
            }
        }
        if (pathbegin != -1){
            Add_Edge(pathend, pathbegin);
        }
        if (!Eulerian_Cycle(path)){
            return 0;
        }
        if (pathbegin != -1){
            edges.pop_back();
            adj[pathend].pop_back();
            deg[pathend]++; deg[pathbegin]--;
            ptr[pathend]--;
            path.pop_back();
            For(i, 0, isz(path) - 1){
                if (path[i] == pathend && path[i + 1] == pathbegin){
                    rotate(path.begin(), path.begin() + i + 1, path.end());
                    break;
                }
            }
        }
        ForE(i, 1, n){
            if (ptr[i] != isz(adj[i])){
                return 0;
            }
        }
        return 1;
    }
};

struct Eulerian_Graph_Undir{
    struct Edge{
        int u, v;
        bool reach;
        
        Edge(int u, int v, bool reach): u(u), v(v), reach(reach) {
            
        }
    };
    
    int n;
    vector <Edge> edges;
    vvi adj;
    vi deg, ptr;
    
    Eulerian_Graph_Undir(int n = 0): n(n) {
        adj.resize(n + 1);
        deg.resize(n + 1);
        ptr.resize(n + 1);
    }

    void Add_Edge(int u, int v){
        edges.push_back(Edge(u, v, 0));
        edges.push_back(Edge(v, u, 0));
        adj[u].push_back(isz(edges) - 2);
        adj[v].push_back(isz(edges) - 1);
        deg[u]++; deg[v]++;
    }

    void Dfs_Eulerian_Undir(int u, vi& path){
        while (ptr[u] < isz(adj[u])){
            ptr[u]++;
            int i = adj[u][ptr[u] - 1];
            if (edges[i].reach){
                continue;
            }
            edges[i].reach = 1;
            edges[i ^ 1].reach = 1;
            Dfs_Eulerian_Undir(edges[i].v, path);
        }
        path.push_back(u);
        return;
    }

    bool Eulerian_Cycle(vi& path){
        ForE(i, 1, n){
            if (deg[i] % 2){
                return 0;
            }
        }
        ForE(i, 1, n){
            if (isz(adj[i])){
                Dfs_Eulerian_Undir(i, path);
                break;
            }
        }
        ForE(i, 1, n){
            if (ptr[i] != isz(adj[i])){
                return 0;
            }
        }
        reverse(bend(path));
        return 1;
    }

    bool Eulerian_Path(vi& path){
        int pathbegin = -1, pathend = -1;
        ForE(i, 1, n){
            if (deg[i] % 2){
                if (pathbegin == -1){
                    pathbegin = i;
                    continue;
                }
                if (pathend == -1){
                    pathend = i;
                    continue;
                }
                return 0;
            }
        }
        if (pathbegin != -1){
            Add_Edge(pathend, pathbegin);
        }
        if (!Eulerian_Cycle(path)){
            return 0;
        }
        if (pathbegin != -1){
            edges.pop_back();
            edges.pop_back();
            adj[pathend].pop_back();
            adj[pathbegin].pop_back();
            deg[pathend]--; deg[pathbegin]--;
            ptr[pathend]--; ptr[pathbegin]--;
            path.pop_back();
            For(i, 0, isz(path) - 1){
                if (path[i] == pathend && path[i + 1] == pathbegin){
                    rotate(path.begin(), path.begin() + i + 1, path.end());
                    break;
                }
            }
        }
        ForE(i, 1, n){
            if (ptr[i] != isz(adj[i])){
                return 0;
            }
        }
        return 1;
    }
};