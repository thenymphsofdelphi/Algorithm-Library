struct Dinic{
    const int inf = 1e9 + 7;
    
    struct Edge{
        int u, v, cap, res_cap;
        
        Edge(int u, int v, int cap): u(u), v(v), cap(cap), res_cap(0) {
            
        }
    };
    
    int n;
    vector <Edge> edges;
    vvi adj;
    vi level, ptr;
    vector <bool> reach;
    
    Dinic(int n): n(n) {
        adj.resize(n + 1);
        level.resize(n + 1);
        ptr.resize(n + 1);
        reach.resize(n + 1);
    }
    
    void Add_Edge_Dir(int u, int v, int w){
        edges.push_back(Edge(u, v, w));
        edges.push_back(Edge(v, u, 0));
        adj[u].push_back(isz(edges) - 2);
        adj[v].push_back(isz(edges) - 1);
    }
    
    void Add_Edge_Undir(int u, int v, int wu, int wv){
        edges.push_back(Edge(u, v, wu));
        edges.push_back(Edge(v, u, wv));
        adj[u].push_back(isz(edges) - 2);
        adj[v].push_back(isz(edges) - 1);
    }
    
    bool Bfs_Flow(int s, int t){
        fill(bend(level), -1);
        level[s] = 0;
        queue <int> qu;
        qu.push(s);
        while (!qu.empty()){
            int u = qu.front(); qu.pop();
            Fora(i, adj[u]){
                if (level[edges[i].v] == -1 && edges[i].res_cap){
                    level[edges[i].v] = level[u] + 1;
                    qu.push(edges[i].v);
                }
            }
        }
        return level[t] != -1;
    }
    
    int Dfs_Flow(int u, int flow, int t){
        if (!flow){
            return 0;
        }
        if (u == t){
            return flow;
        }
        for (; ptr[u] < isz(adj[u]); ptr[u]++){
            int i = adj[u][ptr[u]];
            if (level[edges[i].v] != level[u] + 1 || !edges[i].res_cap){
                continue;
            }
            int new_flow = Dfs_Flow(edges[i].v, min(flow, edges[i].res_cap), t);
            if (!new_flow){
                continue;
            }
            edges[i].res_cap -= new_flow;
            edges[i ^ 1].res_cap += new_flow;
            return new_flow;
        }
        return 0;
    }
    
    int Max_Flow(int s, int t){
        Fora(&i, edges){
            i.res_cap = i.cap;
        }
        int flow = 0, new_flow;
        while (1){
            if (!Bfs_Flow(s, t)){
                break;
            }
            fill(bend(ptr), 0);
            while ((new_flow = Dfs_Flow(s, inf, t))){
                flow += new_flow;
            }
        }
        return flow;
    }
    
    void Dfs_Cut(int u){
        reach[u] = 1;
        Fora(i, adj[u]){
            if (!reach[edges[i].v] && edges[i].res_cap){
                Dfs_Cut(edges[i].v);
            }
        }
    }
    
    int Min_Cut(int s, int t, vpii& cut){
        fill(bend(reach), 0);
        int flow = Max_Flow(s, t);
        Dfs_Cut(s);
        Fora(i, edges){
            if (reach[i.u] && !reach[i.v]){
                cut.push_back(make_pair(i.u, i.v));
            }
        }
        return flow;
    }
};
