struct Edmond_Karp{
    const int inf = 1e9 + 7;
    
    struct Edge{
        int u, v, cap, res_cap;
        
        Edge(int _u, int _v, int _cap): u(_u), v(_v), cap(_cap), res_cap(0) {
            
        }
    };
    
    int n;
    vector <Edge> edges;
    vvi adj;
    vi trace;
    vector <bool> reach;
    
    Edmond_Karp(int n): n(n) {
        adj.resize(n + 1);
        trace.resize(n + 1);
        reach.resize(n + 1);
    }
    
    void Add_Edge_Dir(int u, int v, int w){
        edges.push_back(Edge(u, v, w));
        edges.push_back(Edge(v, u, 0));
        adj[u].push_back(isz(edges) - 2);
        adj[v].push_back(isz(edges) - 1);
    }
    
    void Add_Edge_Undir(int u, int v, int wu, int wv){
        edges.push_back(Edge(u, v, wu));
        edges.push_back(Edge(v, u, wv));
        adj[u].push_back(isz(edges) - 2);
        adj[v].push_back(isz(edges) - 1);
    }
    
    int Bfs_Flow(int s, int t){
        fill(bend(trace), -1);
        trace[s] = -2;
        queue <pii> qu;
        qu.push(make_pair(s, inf));
        while (!qu.empty()){
            int u = qu.front().fi, flow = qu.front().se; qu.pop();
            Fora(i, adj[u]){
                if (trace[edges[i].v] == -1 && edges[i].res_cap){
                    trace[edges[i].v] = i;
                    int new_flow = min(flow, edges[i].res_cap);
                    if (edges[i].v == t){
                        return new_flow;
                    }
                    qu.push(make_pair(edges[i].v, new_flow));
                }
            }
        }
        return 0;
    }
    
    int Max_Flow(int s, int t){
        Fora(&i, edges){
            i.res_cap = i.cap;
        }
        int flow = 0, new_flow;
        while ((new_flow = Bfs_Flow(s, t))){
            flow += new_flow;
            int cur = t;
            while (cur != s){
                edges[trace[cur]].res_cap -= new_flow;
                edges[trace[cur] ^ 1].res_cap += new_flow;
                cur = edges[trace[cur]].u;
            }
        }
        return flow;
    }
    
    void Dfs_Cut(int u){
        reach[u] = 1;
        Fora(i, adj[u]){
            if (!reach[edges[i].v] && edges[i].res_cap){
                Dfs_Cut(edges[i].v);
            }
        }
    }
    
    int Min_Cut(int s, int t, vpii& cut){
        fill(bend(reach), 0);
        int flow = Max_Flow(s, t);
        Dfs_Cut(s);
        Fora(&i, edges){
            if (reach[i.u] && !reach[i.v]){
                cut.push_back(make_pair(i.u, i.v));
            }
        }
        return flow;
    }
};
