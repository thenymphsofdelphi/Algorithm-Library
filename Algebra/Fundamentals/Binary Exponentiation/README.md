# Binary Exponentiation

Binary exponentiation is a trick which allows us to calculate $`a^n`$ in $`\mathcal{O}(\log n)`$ instead of $`O(n)`$.

This trick can also be applied to operations $`\circ`$ that have the property of **associativity**: $`(a \circ b) \circ c = a \circ (b \circ c)`$.

## Algorithm

Let's analyze the problem: Calculate $`a^n`$.

Aside from the obvious naive loop solution, we can think in the way of divide-and-conquer. We can split the problem $`a^n`$ into two subproblems, $`a^x`$ and $`a^{n - x}`$ $`(1 \leq x \leq n - 1)`$. The base case is obviously $`a^0 = 1`$ and $`a^1 = a`$. The pseudocode is as follow:

<details>
    <summary> Pseudocode </summary>

```cpp
int power(int a, int n) {
    // Base cases
    if (n == 0) {
        return 1;
    }
    if (n == 1) {
        return a;
    }
    // Choose x randomly between 1 and n - 1
    int x = rand() % (n - 1) + 1;
    int ans1 = power(a, x), ans2 = power(a, n - x);
    return ans1 * ans2;
}
```
</details>

The above algorithm still has worst case of $`\mathcal{O}(n)`$. However, we haven't fully optimized it yet; specifically, $`x`$ is randomly chosen. Notice that if $`x = \frac{n}{2}`$, then $`a^n = a^{\frac{n}{2} \cdot 2} = \left( a^{\frac{n}{2}} \right)^2 = \left( a^x \right)^2`$. That means we only need to know $`a^{\frac{n}{2}}`$ in order to know $`a^n`$.

That's all good when $`n`$ is even, but how about odd $`n`$? Well, if $`n`$ is of the form $`2k + 1`$, then $`a^{2k + 1}=a^{2k} \cdot a = \left( a^{k} \right)^2 \cdot a`$. So we still only need to know $`a^k`$.

Finally, we have this short formula:
```math
a^n =
\begin{cases}
\left( a^{\frac{n}{2}} \right)^2 &\text{if} \: n \: \text{is even} \\
\left( a^{\left \lfloor \frac{n}{2} \right \rfloor} \right)^2 \cdot a &\text{if} \: n \: \text{is odd}
\end{cases}
```

<details>
    <summary> Pseudocode </summary>

```cpp
int power(int a, int n) {
    // Base cases
    if (n == 0) {
        return 1;
    }
    if (n == 1) {
        return a;
    }
    // If n is even
    if (n % 2 == 0) {
        int x = n / 2;
        int temp = power(a, x);
        temp = temp * temp;
        return temp;
    }
    // If n is odd
    else {
        int x = (n - 1) / 2;
        int temp = power(a, x);
        temp = temp * temp;
        temp = temp * a;
        return temp;
    }
}
```
</details>
