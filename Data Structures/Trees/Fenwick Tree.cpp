struct fenwick_tree{
    vi bit;
    int n;

    fenwick_tree(int n = 0){
        this->n = n + 1;
        bit.assign(this->n + 1, 0);
    }

    fenwick_tree(vector<int> a) : fenwick_tree(a.size()){
        this->n = a.size() + 1;
        bit.assign(n + 1, 0);
        For(i, 0, a.size()){
            update(i, a[i]);
        }
    }

    int get(int idx){
        int ans = 0;
        for (; idx > 0; idx -= idx & -idx){
            ans += bit[idx];
        }
        return ans;
    }

    int get(int l, int r){
        return get(r) - get(l - 1);
    }

    void update(int idx, int delta){
        for (; idx <= n; idx += idx & -idx){
            bit[idx] += delta;
        }
    }
} bit1;

struct fenwick_tree_2d{
    vvi bit;
    int n, m;

    fenwick_tree_2d(int n = 0, int m = 0){
        this->n = n + 1;
        this->m = m + 1;
        bit.assign(this->n + 1, vi(this->m + 1, 0));
    }

    fenwick_tree_2d(const vvi& a){
        this->n = a.size() + 1;
        this->m = a[0].size() + 1;
        bit.assign(n + 1, vi(m + 1, 0));
        For(i, 0, a.size()){
            For(j, 0, a[0].size()){
                update(i, j, a[i][j]);
            }
        }
    }

    int get(int x, int y){
        int ans = 0;
        for (int i = x; i > 0; i -= i & -i){
            for (int j = y; j > 0; j -= j & -j){
                ans += bit[i][j];
            }
        }
        return ans;
    }

    int get(int x1, int y1, int x2, int y2){
        return get(x2, y2) - get(x2, y1 - 1) - get(x1 - 1, y2) + get(x1 - 1, y1 - 1);
    }

    void update(int x, int y, int delta){
        for (int i = x; i <= n; i += i & -i){
            for (int j = y; j <= m; j += j & -j){
                bit[i][j] += delta;
            }
        }
    }
} bit2;

int n, m;

void update(int ux, int uy, int vx, int vy, int val){
    a.update(ux, uy, val);
    a.update(ux, vy + 1, -val);
    a.update(vx + 1, uy, -val);
    a.update(vx + 1, vy + 1, val);
    b.update(ux, uy, -(uy - 1) * val);
    b.update(ux, vy + 1, vy * val);
    b.update(vx + 1, uy, (uy - 1) * val);
    b.update(vy + 1, vy + 1, -vy * val);
    c.update(ux, uy, -(ux - 1) * val);
    c.update(ux, vy + 1, (ux - 1) * val);
    c.update(vx + 1, uy, vx * val);
    c.update(vy + 1, vy + 1, -vx * val);
    d.update(ux, uy, (ux - 1) * (uy - 1) * val);
    d.update(ux, vy + 1, (ux - 1) * -vy * val);
    d.update(vx + 1, uy, -vx * (uy - 1) * val);
    d.update(vx + 1, vy + 1, vx * vy * val);
}

int get(int x, int y){
    if (x <= 0 || y <= 0) return 0;
    return x * y * a.get(x, y) + x * b.get(x, y) + y * c.get(x, y) + d.get(x, y);
}

int get(int ux, int uy, int vx, int vy){
    return get(vx, vy) - get(vx, uy - 1) - get(ux - 1, vy) + get(ux - 1, uy - 1);
}
