struct disjoint_set_union{
    int par[N];
    
    void init(){
        fill(par, par + N, -1);
    }

    int root(int x){
        return par[x] < 0 ? x : par[x] = root(par[x]);
    }

    void merge(int x, int y){
        if ((x = root(x)) == (y = root(y))){
            return;
        }
        if (par[y] < par[x]){
            swap(x, y);
        }
        par[x] += par[y];
        par[y] = x;
    }
} dsu;

struct weighted_disjoint_set_union{
    pii par[N];
    
    void init(){
        fill(par, par + N, make_pair(-1, 0));
    }

    pii root(int x){
        if (par[x].fi < 0){
            return make_pair(x, 0);
        }
        pii ans = root(par[x].fi);
        ans.se += par[x].se;
        return (par[x] = ans);
    }

    void merge(int x, int y, int d){
        if ((x = root(x).fi) == (y = root(y).fi)){
            return;
        }
        if (par[y].fi < par[x].fi){
            swap(x, y);
        }
        par[x].fi += par[y].fi;
        par[y].fi = x;
        par[y].se = d;
    }
} dsu2;
