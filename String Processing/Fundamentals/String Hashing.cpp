const int N = 1e5 + 5, mod1 = 1e9 + 7, mod2 = 998244353;
const int base = 31;

int add(int a, int b, int mod){
    a += b;
    if (a >= mod){
        a -= mod;
    }
    return a;
}

int sub(int a, int b, int mod){
    a -= b;
    if (a < 0){
        a += mod;
    }
    return a;
}

int mul(int a, int b, int mod){
    return (ll)a * b % mod;
}

int n;
string s;
pii hashs[N];
int pw1[N], pw2[N];

void init(){
    s = ' ' + s;
    pw1[0] = 1;
    pw2[0] = 1;
    For(i, 1, N){
        pw1[i] = mul(pw1[i - 1], base, mod1);
        pw2[i] = mul(pw2[i - 1], base, mod2);
    }
    ForE(i, 1, n){
        hashs[i] = {add(mul(hashs[i - 1].fi, base, mod1), s[i] - 'a' + 1, mod1), add(mul(hashs[i - 1].se, base, mod2), s[i] - 'a' + 1, mod2)};
    }
}

pii cal(int l, int r){
    pii ans = make_pair(-1, -1);
    ans.fi = sub(hashs[r].fi, mul(hashs[l - 1].fi, pw1[r - l + 1], mod1), mod1);
    ans.se = sub(hashs[r].se, mul(hashs[l - 1].se, pw2[r - l + 1], mod2), mod2);
    return ans;
}
